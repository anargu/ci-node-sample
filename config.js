const config = {
    app: {
      port: 3000
    },
    api_ruc:{
      endpoint: 'https://api.sunat.cloud/ruc/'
    },
    api_dni:{
      endpoint: 'http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI='
    }
   };
   
   module.exports = config;